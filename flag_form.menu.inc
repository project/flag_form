<?php

/**
 *  @file
 *  Menu page callbacks for Flag Form.
 */

/**
 *  Callback for admin/build/flags/flag-form.
 */
function flag_form_page() {
  $form = array();
  $form['flag_form_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Flag form title'),
    '#description' => t('This is the title that will be displayed for flag forms.'),
    '#default_value' => variable_get('flag_form_title', t('Flags')),
  );
  return system_settings_form($form);
}

