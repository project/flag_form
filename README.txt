
Flag Form
=========

This will display selected node flags within a form of checkboxes, rather than
as links. This allows multiple flags to be marked simultaneously.
